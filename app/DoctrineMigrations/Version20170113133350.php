<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170113133350 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE social_page (id INT AUTO_INCREMENT NOT NULL, firstName VARCHAR(100) NOT NULL, lastName VARCHAR(100) NOT NULL, middleName VARCHAR(100) NOT NULL, birthDate INT NOT NULL, country VARCHAR(100) NOT NULL, city VARCHAR(100) NOT NULL, sex TINYINT(1) NOT NULL, pets TINYINT(1) NOT NULL, hobby LONGTEXT NOT NULL, children TINYINT(1) NOT NULL, avatar VARCHAR(255) NOT NULL, aboutMe LONGTEXT NOT NULL, statusMessage VARCHAR(255) NOT NULL, yourFavoriteSong VARCHAR(255) NOT NULL, yourFavoriteFilms VARCHAR(255) NOT NULL, friends VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Test (id INT AUTO_INCREMENT NOT NULL, topic VARCHAR(100) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE social_page');
        $this->addSql('DROP TABLE Test');
    }
}
