<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Country;

class CountryFixture extends AbstractFixture implements OrderedFixtureInterface
{
    const COUNTRIES = [
        'Belarus', 'Ukraine', 'Russia', 'USA', 'Canada', 'Romania', 'Hungary'
    ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::COUNTRIES as $someItem) {
            $country = new Country();
            $country->setCtrName($someItem);
            $manager->persist($country);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        // TODO: Implement getOrder() method.
        return 1;
    }
}