<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.01.2017
 * Time: 17:22
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="country")
 */
class Country
{
    /**
     * @ORM\Column(name="Ctr_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Ctr_Id;

    /**
     * @ORM\Column(name="Crt_Name", type="string", length=100)
     * @Assert\NotBlank
     *
     */
    private $Ctr_Name;


    /**
     * Get ctrId
     *
     * @return integer
     */
    public function getCtrId()
    {
        return $this->Ctr_Id;
    }

    /**
     * Set ctrName
     *
     * @param string $ctrName
     *
     * @return Country
     */
    public function setCtrName($ctrName)
    {
        $this->Ctr_Name = $ctrName;

        return $this;
    }

    /**
     * Get ctrName
     *
     * @return string
     */
    public function getCtrName()
    {
        return $this->Ctr_Name;
    }
}
