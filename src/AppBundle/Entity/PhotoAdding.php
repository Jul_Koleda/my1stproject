<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="photo_adding")
 */
class PhotoAdding
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $photo_id;
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload a photo as a JPEG(JPG) file.")
     * @Assert\File(mimeTypes={ "image/jpeg"})
     */
    private $photo;


    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return PhotoAdding
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Get photoId
     *
     * @return integer
     */
    public function getPhotoId()
    {
        return $this->photo_id;
    }
}
