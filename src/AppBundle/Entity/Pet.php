<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.01.2017
 * Time: 13:47
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="pet")
 */
class Pet
{
    /**
     * @ORM\Column(name="Pet_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
private $id;

    /**
     * @ORM\Column(name="Pet_Name", type="string", length=100)
     * @Assert\NotBlank
     *
     */
private $petName;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set petName
     *
     * @param string $petName
     *
     * @return Pet
     */
    public function setPetName($petName)
    {
        $this->petName = $petName;

        return $this;
    }

    /**
     * Get petName
     *
     * @return string
     */
    public function getPetName()
    {
        return $this->petName;
    }
}
