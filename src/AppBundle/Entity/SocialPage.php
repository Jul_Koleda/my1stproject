<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="social_page")
 */
class SocialPage
{
    const PETS = [
        1 => 'Cat',
        2 => 'Dog',
        3 => 'Spider',
        4 => 'Bird'
    ];

    const COUNTRY = [
        1 => 'Ukraine',
        2 => 'Belarus',
        3 => 'Russia',
        4 => 'USA',
        5 => 'Romania',
        6 => 'Hungary'
    ];

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $middleName;

    /**
     * @ORM\Column(type="date")
//     * @Assert\NotBlank
     */
    private $birthDate;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    private $sex;


    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $pet;

    /**
     * @ORM\Column(type="text")
     */
    private $hobby;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    private $children;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload an Avatar as a JPG (JPEG) file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $avatar;


    /**
     * @ORM\Column(type="text")
     */
    private $aboutMe;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $statusMessage;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $yourFavoriteSong;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $yourFavoriteFilms;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $friends;

    public function __construct()
    {
        $this->country = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return SocialPage
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return SocialPage
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     *
     * @return SocialPage
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }


    /**
     * Set country
     *
     * @param string $country
     *
     * @return SocialPage
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return SocialPage
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set sex
     *
     * @param boolean $sex
     *
     * @return SocialPage
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return boolean
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set hobby
     *
     * @param string $hobby
     *
     * @return SocialPage
     */
    public function setHobby($hobby)
    {
        $this->hobby = $hobby;

        return $this;
    }

    /**
     * Get hobby
     *
     * @return string
     */
    public function getHobby()
    {
        return $this->hobby;
    }

    /**
     * Set children
     *
     * @param boolean $children
     *
     * @return SocialPage
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return boolean
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return SocialPage
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set aboutMe
     *
     * @param string $aboutMe
     *
     * @return SocialPage
     */
    public function setAboutMe($aboutMe)
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    /**
     * Get aboutMe
     *
     * @return string
     */
    public function getAboutMe()
    {
        return $this->aboutMe;
    }

    /**
     * Set statusMessage
     *
     * @param string $statusMessage
     *
     * @return SocialPage
     */
    public function setStatusMessage($statusMessage)
    {
        $this->statusMessage = $statusMessage;

        return $this;
    }

    /**
     * Get statusMessage
     *
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * Set yourFavoriteSong
     *
     * @param string $yourFavoriteSong
     *
     * @return SocialPage
     */
    public function setYourFavoriteSong($yourFavoriteSong)
    {
        $this->yourFavoriteSong = $yourFavoriteSong;

        return $this;
    }

    /**
     * Get yourFavoriteSong
     *
     * @return string
     */
    public function getYourFavoriteSong()
    {
        return $this->yourFavoriteSong;
    }

    /**
     * Set yourFavoriteFilms
     *
     * @param string $yourFavoriteFilms
     *
     * @return SocialPage
     */
    public function setYourFavoriteFilms($yourFavoriteFilms)
    {
        $this->yourFavoriteFilms = $yourFavoriteFilms;

        return $this;
    }

    /**
     * Get yourFavoriteFilms
     *
     * @return string
     */
    public function getYourFavoriteFilms()
    {
        return $this->yourFavoriteFilms;
    }

    /**
     * Set friends
     *
     * @param string $friends
     *
     * @return SocialPage
     */
    public function setFriends($friends)
    {
        $this->friends = $friends;

        return $this;
    }

    /**
     * Get friends
     *
     * @return string
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return SocialPage
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set pet
     *
     * @param integer $pet
     *
     * @return SocialPage
     */
    public function setPet($pet)
    {
        $this->pet = $pet;

        return $this;
    }

    /**
     * Get pet
     *
     * @return integer
     */
    public function getPet()
    {
        return $this->pet;
    }
}
