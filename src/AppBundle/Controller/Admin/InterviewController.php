<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.01.2017
 * Time: 15:30
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Interview;
//use AppBundle\Form\Type\InterviewType;
use AppBundle\Form\Type\InterviewType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class InterviewController
 * @package AppBundle\Controller\Admin
 * @Route("interview")
 */
class InterviewController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | array
     * @Template(":admin/interview:interview.html.twig")
     * @Route("", name="interview")
     */
    public function mainAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $records = $em->getRepository(Interview::class)->findAll();
        $interview = new Interview();

        $form = $this->createForm(InterviewType::class, $interview, []);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($interview);
//            Метод persist выполняет подготовку объекта в базе данных . физически запись в базу происходит при вызове flush)
            $em->flush();
            return $this->redirectToRoute("interview");

        }
        return [
            'interview' => $form->createView(),
            'records' => $records
        ];
    }


}