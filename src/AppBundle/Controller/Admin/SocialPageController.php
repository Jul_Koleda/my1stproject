<?php

namespace AppBundle\Controller\Admin;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\SocialPage;
use AppBundle\Form\Type\SocialPageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class PhotoAddingController
 * @package AppBundle\Controller\Admin
 * @Route("/")
 */
class SocialPageController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | array
     * @Template(":admin/socialPage:socialPage.html.twig")
     * @Route("", name="social_page_list")
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $records = $em->getRepository(SocialPage::class)->findAll();

        return [
            'records' => $records
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | array
     * @Template(":admin/socialPage:new.html.twig")
     * @Route("/add", name="social_page_create")
     */
    public function addAction(Request $request)

    {
        $socialPage = new SocialPage();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(SocialPageType::class, $socialPage, []);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UploadedFile $file */
            $file = $socialPage->getAvatar();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->getParameter('photo_directory'),
                $fileName
            );

            $socialPage->setAvatar($fileName);

            $em->persist($socialPage);
            $em->flush();
            return $this->redirectToRoute("social_page_list");
        }
        return [
            'form' => $form->createView(),
        ];
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | array
     * @Template(":admin/socialPage:edit.html.twig")
     * @Route("/{socialPage}/edit", name="social_page_edit")
     */
    public function editAction(SocialPage $socialPage, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $socialPage->setAvatar(
            new File($this->getParameter('photo_directory').'/'.$socialPage->getAvatar())
        );
        $form = $this->createForm(SocialPageType::class, $socialPage);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UploadedFile $file */
            $file = $socialPage->getAvatar();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->getParameter('photo_directory'),
                $fileName
            );

            $socialPage->setAvatar($fileName);

            $em->persist($socialPage);
            $em->flush();
            return $this->redirectToRoute("social_page_view", [
                'socialPage' => $socialPage->getId()
            ]);
        }
        return [
            'form' => $form->createView(),
        ];
    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | array
     * @Template(":admin/socialPage:view.html.twig")
     * @Route("/{socialPage}/view", name="social_page_view")
     */
    public function viewAction(SocialPage $socialPage, Request $request)
    {
        return [
            'socialPage' => $socialPage,
        ];
    }
}