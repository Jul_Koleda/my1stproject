<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.01.2017
 * Time: 16:00
 */

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PhotoAdding;
use AppBundle\Form\Type\PhotoAddingType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class PhotoAddingController
 * @package AppBundle\Controller\Admin
 * @Route("/photo_adding")
 */
class PhotoAddingController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @package AppBundle\Controller\Admin
     * @Template(":admin/socialPage:new.html.twig")
     * //@Route("/nhjj", "photo_adding")
     */
    public function newAction(Request $request)
    {
        $photo = new PhotoAdding();

        $form = $this->createForm(PhotoAddingType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded jpg file
            $file = $photo->getPhoto();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()) . '.' . $file->GuessExtention();

            // Move the file to the directory where photo are stored
            $file->move(
                $this->getParameter($file), //$file ????
                $fileName
            );
            $photo->setPhoto($fileName);

            // ... persist the $photo variable or any other work

            return $this->redirect($this->generateUrl('%kernel.root_dir%/../web/pics'));
        }

        return $this->render(':admin/socialPage:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}