<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.01.2017
 * Time: 19:37
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Feedback;
use AppBundle\Form\Type\FeedbackType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FeedbackController
 * @package AppBundle\Controller\Admin
 * @Route("feedback")
 */
class FeedbackController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | array
     * @Template(":admin/feedback:feedback.html.twig")
     * @Route("", name="feedback")
     */
    public function mainAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $records = $em->getRepository(Feedback::class)->findAll();
        $feedback = new Feedback();

        $form = $this->createForm(FeedbackType::class, $feedback, []);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($feedback);
            $em->flush();
            return $this->redirectToRoute("feedback");

        }
        return [
            'feedback' => $form->createView(),
            'records' => $records
        ];
    }
}