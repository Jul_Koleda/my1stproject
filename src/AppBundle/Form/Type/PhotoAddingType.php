<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.01.2017
 * Time: 15:48
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\PhotoAdding;
use AppBundle\Entity\SocialPage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class PhotoAddingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('photo', FileType::class,
                ['label' => 'Photo (JPG file)'
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PhotoAdding::class,
        ));
    }
}