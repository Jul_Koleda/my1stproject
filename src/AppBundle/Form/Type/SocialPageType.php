<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.01.2017
 * Time: 12:45
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\SocialPage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SocialPageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'First Name'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'last Name'
            ])
            ->add('middleName', TextType::class, [
                'label' => 'Middle Name'
            ])
            ->add('birthDate', DateType::class,
                [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ],
                    'label' => 'Birthday',
                    'years' => range(1940, 2015),
                ])
            ->add('country', ChoiceType::class, array(
                'choices' => array_flip(SocialPage::COUNTRY),
                'label' => 'Country'
            ))
            ->add('city', TextType::class, [
                'label' => 'City',
            ])
            ->add('sex', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 1,
                    'Female' => 2,
                ),
                'label' => 'Sex',))
            ->add('pet', ChoiceType::class, array(
                'choices' => array_flip(SocialPage::PETS),
                'label' => 'Pets',
            ))
            ->add('hobby', TextareaType::class, [
                'label' => 'Hobby'
            ])
            ->add('children', ChoiceType::class, array(
                'choices' => array(
                    'Yes' => 1,
                    'No' => 2,
                ),
                'label' => 'Children',))
            ->add('aboutMe', TextType::class, [
                'label' => 'About you'
            ])
            ->add('avatar', FileType::class, [
                'label' => 'Avatar',
                'required' => false,
            ])
            ->add('statusMessage', TextType::class, [
                'label' => 'Set a status'
            ])
            ->add('yourFavoriteSong', TextType::class, [
                'label' => 'Your Favorite Song'
            ])
            ->add('yourFavoriteFilms', TextType::class, [
                'label' => 'Your Favorite Films'
            ])
            ->add('friends', TextType::class, [
                'label' => 'Friend list'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SocialPage::class
        ]);
    }
}